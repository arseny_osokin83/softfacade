$(document).ready(function() {
	$('.top-slider').slick({
		dots: true,
		arrows: false,
	});


	$('.footer-menu_mobile li ul').hide();

    $('.footer-menu_mobile li>a').on('click', function() {
    	var element = $(this).parent('li');
    	if (element.hasClass('open')) {
    		element.removeClass('open');
    		element.find('li').removeClass('open');
    		element.find('ul').slideUp();
    	}
    	else {
    		element.addClass('open');
    		element.children('ul').slideDown();
    		element.siblings('li').removeClass('open');
    		element.siblings('li').find('li').removeClass('open');
			element.siblings('li').find('ul').slideUp();
    	}
    });
});